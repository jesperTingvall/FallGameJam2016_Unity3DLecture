﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public CharacterController character;

	public float speed;

	public float angleSpeed;

	public Transform spawnPos;

	public GameObject bulletPrefab;

	public int score;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 move = transform.rotation * new Vector3 (0, 0, Input.GetAxis ("Vertical"));

		character.SimpleMove (move * speed);

		transform.Rotate (0, Input.GetAxis ("Horizontal") * angleSpeed * Time.deltaTime, 0);

		if (Input.GetButtonDown ("Fire1")) {
			Instantiate (bulletPrefab, spawnPos.position, spawnPos.rotation);
		}
	
	}
}
