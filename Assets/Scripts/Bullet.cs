﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	private Rigidbody body;
	public float speed;

	public float lifeTime;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody> ();
		body.velocity = transform.forward * speed;
		Destroy (gameObject, lifeTime);
	}

	void OnCollisionEnter(Collision collider) {
		Debug.Log ("Hitting " + collider.collider.name);

		HealthComponent h = collider.collider.GetComponentInParent<HealthComponent> ();

		if (h) {
			h.Damage (1);
		}


		Destroy (gameObject);

	}


	
	// Update is called once per frame
	void Update () {
	
	}
}
