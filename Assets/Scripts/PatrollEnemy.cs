﻿using UnityEngine;
using System.Collections;

public class PatrollEnemy : MonoBehaviour {

	private NavMeshAgent agent;

	public Transform[] targets;

	public int currentTarget;

	public float dist;

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		NextTarget ();
	}

	void NextTarget() {
		currentTarget = (currentTarget + 1) % targets.Length;
		agent.SetDestination (targets [currentTarget].position);
	}
	
	// Update is called once per frame
	void Update () {

		if ((transform.position - agent.destination).magnitude < dist) {
			NextTarget ();
		}
	
	}
}
