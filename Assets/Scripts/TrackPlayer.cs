﻿using UnityEngine;
using System.Collections;

public class TrackPlayer : MonoBehaviour {

	public NavMeshAgent agent;

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
	
	}
	
	// Update is called once per frame
	void Update () {
		agent.SetDestination (FindObjectOfType<PlayerScript> ().transform.position);
	
	}
}
