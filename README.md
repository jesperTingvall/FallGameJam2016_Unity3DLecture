# Fall Game Jam 2016 Unity 3D lecture
Project repository contains a Unity 3D project detailing Unity 3D basics and navigation using a navmesh.

## Contact & Copyright
Created by Jesper Tingvall for [LiTHe kod](http://lithekod.se/) Feel free to do whatever you want with the included source code. If you need to contact me I can be reached at the following places.

- [tingvall.pw](https://tingvall.pw/)
- [@jesperTingvall](https://twitter.com/jesperTingvall)
- [jesper@tingvall.pw](mailto:jesper@tingvall.pw)